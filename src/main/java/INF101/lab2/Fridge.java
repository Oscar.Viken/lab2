package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.time.LocalDate;



public class Fridge implements IFridge {
    
    ArrayList<FridgeItem> fridge = new ArrayList<>();

    public int nItemsInFridge() {
        return fridge.size();
    
    }

    @Override
    public int totalSize() {
        return 20; 
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (fridge.size() < 20) {
            fridge.add(item);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridge.contains(item)) {
        fridge.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        fridge.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {

        ArrayList<FridgeItem> expiredItems = new ArrayList<FridgeItem>();

        for (FridgeItem item : fridge) {
            if (item.getExpirationDate().isBefore(LocalDate.now())) {
                expiredItems.add(item);
            }
        }
        fridge.removeAll(expiredItems);
        return expiredItems;
    }
    
}
